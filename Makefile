testfiles := $(wildcard tests/*.plt)
version   := $(shell swipl -s pack.pl -g 'version(X), writeln(X).' -t halt)
packname  := $(shell basename $$(pwd))

#.PHONY: test

all:

check: consul_agent_start $(testfiles)

%.plt: FORCE
	swipl -s $@ -g run_tests -t halt

consul_agent_start: FORCE
	consul agent -dev >/dev/null 2>&1 &

release: check make_tgz releasebranch FORCE
	mv -n ../$(packname)-$(version).tgz .
	git add .
	git commit -m "release $(version)"

releasebranch: FORCE
	git checkout releases

make_tgz: FORCE
	rm -f ../$(packname)-$(version).tgz
	find ../$(packname) -name '*.pl' -o -name LICENSE |sed -e 's/^...//'|xargs tar cvzfp ../$(packname)-$(version).tgz -C ..

install:

FORCE:
