/*
  BSD 2-Clause License

  Copyright (c) 2018, Can Bican
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
:- begin_tests(consul).
:- use_module(prolog/consul_client).

test_create(Object, Options, Result) :-
  uuid(Key),
  update_consul_key(Key, Object),
  read_consul_key(Key, Result, Options),
  delete_consul_key(Key).

test(create_default, Result == '123') :- test_create('123', [], Result).
test(create_atom, Result == '123') :- test_create('123', [as(atom)], Result).
test(create_number, Result == 123) :- test_create(123, [as(number)], Result).
test(create_string, Result == "123") :- test_create(123, [as(string)], Result).
test(create_list, Result == [1, 2, 3]) :- test_create([1, 2, 3], [as(list)], Result).

test(update, Result == a124) :-
  uuid(Key),
  update_consul_key(Key, a123),
  update_consul_key(Key, a124),
  read_consul_key(Key, Result),
  delete_consul_key(Key).

test(read_absent, fail) :-
  uuid(Key),
  delete_consul_key(Key),
  read_consul_key(Key, _).

test(delete_absent) :-
  uuid(Key),
  delete_consul_key(Key),
  delete_consul_key(Key).

test(services, true(member(consul, Services))) :-
  consul_services(Services).

test(service, Properties == [service{host:"",port:8300,tags:[]}]) :-
  consul_service(consul, Properties).

test(register_service, Result = [service{host:"10.0.0.1",port:9000,tags:["a","b"]}]) :-
  uuid(UUID),
  register_consul_service(UUID, '10.0.0.1', 9000, [a, b]),
  consul_service(UUID, Result),
  deregister_consul_service(UUID).

test(deregister_service, fail) :-
  uuid(UUID),
  register_consul_service(UUID, '10.0.0.1', 9000, [a, b]),
  deregister_consul_service(UUID),
  consul_service(UUID, _).

:- end_tests(consul).
