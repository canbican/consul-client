## [0.0.3] 2018-06-16

* Pack file is smaller now.

## [0.0.2] 2018-06-16

* Fixed download link.

## [0.0.1] 2018-06-15

* Initial release.
