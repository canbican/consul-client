/*
  BSD 2-Clause License

  Copyright (c) 2018, Can Bican
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
:- module(consul, [
     read_consul_key/2,
     read_consul_key/3,
     update_consul_key/2,
     delete_consul_key/1,
     consul_services/1,
     consul_service/2,
     register_consul_service/4,
     deregister_consul_service/1
   ]).

/** <module> Consul client

An interface to consul:

  * Key/value storage can be manipulated by read_consul_key/3, update_consul_key/2 and delete_consul_key/1 predicates.
  * Services can be queried by consul_services/1 and consul_service/2 predicates.
  * New services can be registered by register_consul_service/4 predicate.
  * Service can be deregistered by deregister_consul_service/1 predicate.

The module requires a local consul agent running on the default port.

@see https://www.consul.io/
@author Can Bican
@license BSD 2-clause

*/

:- use_module(library(http/http_client)).
:- use_module(library(http/http_json)).
:- use_module(library(http/json)).

connection('http://localhost:8500/v1').
kv_url(Key, URL) :- connection(Connection), format(atom(URL), '~w/kv/~w', [ Connection, Key ]).
svs_url(URL) :- connection(Connection), format(atom(URL), '~w/catalog/services', [ Connection ]).
sv_url(Service, URL) :- connection(Connection), format(atom(URL), '~w/catalog/service/~w', [ Connection, Service ]).
reg_url(URL) :- connection(Connection), format(atom(URL), '~w/agent/service/register', [ Connection ]).
dereg_url(Service, URL) :- connection(Connection), format(atom(URL), '~w/agent/service/deregister/~w', [ Connection, Service ]).

%! consul_services(-Services:list) is semidet
%
% Lists services registered on consul. This only lists the service names, further querying should be performed by consul_service/2.
%
% @arg Services List of strings, all registered services on the consul agent.
%
consul_services(Services) :-
  svs_url(URL),
  http_get(URL, Result, [content_type('application/json'), json_object(dict)]),
  findall(X, _ = Result.get(X), Services).

%! consul_service(+Service:atom, -Properties:dict) is semidet
%
% Lists properties of a service registered on consul. Result is a dict of properties:
%
% @arg Service the name of the service, as received from consul_services/1.
% @arg Properties the properties of the queried service, as a dict of:
%
% * *host*: IP address of the service.
% * *port*: Port of the service.
% * *tags*: Tags associated with the service, as a list.
%
consul_service(Service, Properties) :-
  sv_url(Service, URL),
  http_get(URL, Result, [content_type('application/json'), json_object(dict)]),
  convlist([X,Y]>>(
    get_dict('ServiceAddress', X, Address),
    get_dict('ServicePort', X, Port),
    get_dict('ServiceTags', X, Tags),
    Y = service{host: Address, port: Port, tags: Tags}
  ), Result, PropertiesFound),
  (
    PropertiesFound == [], !, fail ; Properties = PropertiesFound
  ).

%! read_consul_key(+Key:atom, -Value:atom) is semidet
%
% Same as read_consul_key(Key, Value, [as(atom)]).
%
% @see read_consul_key/3
%
read_consul_key(Key, Value) :-
  read_consul_key(Key, Value, []).

%! read_consul_key(+Key:atom, -Value:atom, +Options:options) is semidet
%
% Reads a key from consul kv database
%
% @arg Key Name of the key to query for.
% @arg Value Value of the queried key.
% @arg Options Provided options are:
%
% * *as(+Type)* Type of the value. Type can be =list=, =string=, =number= or =atom=.
%
read_consul_key(Key, Value, Options) :-
  catch(
    (
      kv_url(Key, URL),
      http_get(URL, [Data|_], [content_type('application/json'), json_object(dict)]),
      base64(V, Data.'Value'),
      option(as(Type), Options, atom),
      convert_result(V, Type, Value)
    ),
    error(existence_error(url,_),context(_,status(404,_))),
    fail
  ).

convert_result(In, atom, In) :- !.
convert_result(In, number, Out) :- atom_number(In, Out), !.
convert_result(In, string, Out) :- atom_string(In, Out), !.
convert_result(In, list, Out) :-
  atom_string(In, Ins),
  format(string(S), "lst(~w).", Ins),
  open_string(S, Stream),
  read_term(Stream, lst(Out), []),
  !.

%! delete_consul_key(+Key:atom) is det
%
% Deletes a key from consul kv database
%
% @arg Key name of the key to delete
%
delete_consul_key(Key) :-
  kv_url(Key, URL),
  http_delete(URL, _, [content_type('application/json'), json_object(dict)]).

%! update_consul_key(+Key:atom, +Value:atom) is det
%
% Creates or updates a key in consul kv database
%
% @arg Key name of the key to update
% @arg Value new value of the key
%
update_consul_key(Key, Value) :-
  is_list(Value),
  kv_url(Key, URL),
  format(string(ValueToSend), "~w", [Value]),
  http_put(URL, atom(ValueToSend), _, [content_type('application/json')]),
  !.
update_consul_key(Key, Value) :-
  kv_url(Key, URL),
  http_put(URL, atom(Value), _, [content_type('application/json')]).

%! register_consul_service(+Service: atom, +Host: atom, +Port: integer, +Tags: list) is det
%
% Registers a service in consul or updates the existing service
%
% @arg Service service name to register
% @arg Host IP address of the service
% @arg Port port of the service
% @arg Tags tags associated with the service
register_consul_service(Service, Host, Port, Tags) :-
  Payload = _{ 'Name': Service, 'Tags': Tags, 'Address': Host, 'Port': Port},
  with_output_to(string(PayloadJson), json_write(current_output, Payload, [])),
  reg_url(URL),
  http_put(URL, codes(PayloadJson), _, []).

%! deregister_consul_service(+Service: atom) is det
%
% Deregisters a service in consul
%
% @arg Service service name to deregister
deregister_consul_service(Service) :-
  dereg_url(Service, URL),
  http_put(URL, _, _, []).
