name(consul_client).
title('Consul Client').
version('0.0.4').
author('Can Bican', 'can@bican.net').
packager('Can Bican', 'can@bican.net').
maintainer('Can Bican', 'can@bican.net').
home('https://gitlab.com/canbican/consul-client').
download('https://gitlab.com/canbican/consul-client/raw/releases/consul_client-0.0.2.tgz').
